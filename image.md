# 图像生成

## 获取模型列表

### 基础模型（Checkpoint）
```http request
GET https://p0.kamiya.dev/api/image/listBaseModel
````
响应示例
```json
[
    "anything-v3.0",
    "anything-v4.0",
    "anything-v4.5",
    "anything-v5-PrtRE",
    "pastelMixStylizedAnime"
]
```
## 图像生成
```http request
POST https://p0.kamiya.dev/api/image/generate
```
请求示例
```json
{
    "prompt":"1 girl...",
    "image": "data:image/png;base64,/9j/4AAQSkZJRgABAQEASABIAA...",// 可选 用于i2i base64 url
    "negativePrompt":"lowres, bad anatomy, bad hands, text, error, missing fingers, extra digit, fewer digits, cropped, worst quality, low quality, normal quality, jpeg artifacts, signature, watermark, username, blurry",
    "steps":28,
    "scale":12,
    "seed":2376925602,
    "sampler":"DPM++ 2M Karras",
    "width":768,
    "height":512,
    "traceId":"ef7746b6-4e87-42cb-8329-a463521e6d14", // 可选
    "watermark":false,
    "model":"anything-v3.0"
}
```
响应示例
```json
{
    "status": 200,
    "message": "OK",
    "cost": 1,
    "image": "https://r2.kamiya-a.tech/usercontent/c02ebe81-9677-41d0-8890-f77bdf627984",
    "backend": "Kamiya Backend Group #Lora-1 RTX 4090"
}
```
## 任务追踪
如果在请求中指定了`traceId`，则可以通过以下接口获取任务状态
```http request
GET https://p0.kamiya.dev/api/image/trace?traceId=ef7746b6-4e87-42cb-8329-a463521e6d14
```
响应示例
```json
{
    "status":200,
    "message":"OK",
    "progress":0.9385714285714286
}
```
