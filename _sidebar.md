* 入门

    * [快速开始](start.md)

* API

    * [鉴权](auth.md)
    * [图像生成](image.md)
    * [OpenAI](openai.md)
    * [QR Waifu](qrwaifu.md)
    * [生成历史](history.md)
    * [计费历史](billinghistory.md)

* OpenID App

  * [简介](openid.md)
  * [接口文档](openiddoc.md)
