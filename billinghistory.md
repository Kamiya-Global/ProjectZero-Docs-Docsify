# 计费历史

可以通过该API获取账户的计费历史。

## 获取计费历史列表
```http request
GET https://p0.kamiya.dev/api/billing/history?start=1&take=2
```
响应示例
```json
{
    "status": 200,
    "message": "OK",
    "data": [
        {
            "id": "1p9w7",
            "amount": 1,
            "reason": "chatCompletion-Legacy",
            "createdAt": "07-10 03:59:33"
        },
        {
            "id": "zq14a",
            "amount": 1,
            "reason": "chatCompletion-Legacy",
            "createdAt": "07-10 03:59:32"
        }
    ]
}
```
