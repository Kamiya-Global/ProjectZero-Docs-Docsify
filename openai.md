# OpenAI
Kamiya API与OpenAI官方API完全兼容，您可以将该接口作为中继直接用于像 [BetterChatGPT](https://github.com/ztjhz/BetterChatGPT) 这样为OpenAI API开发的应用程序。

## Chat Completion
```http request
POST https://p0.kamiya.dev/api/openai/chat/completions
```
Stream调用示例(SSE)
```json
{
    "messages": [
        {
          "role":"system",
          "content":"You are ChatGPT, a large language model trained by OpenAI.\nCarefully heed the user's instructions. \nRespond using Markdown."
        },
        {
          "role":"user",
          "content":"你好"
        }
    ],
    "model":"openai:gpt-3.5-turbo",
    "max_tokens":null,
    "temperature":1,
    "presence_penalty":0,
    "top_p":1,
    "frequency_penalty":0,
    "stream":true
}
```
阻塞式调用示例(非SSE)
```json
{
    "messages": [
        {
          "role":"user",
          "content":"Generate a title in less than 6 words for the following message (language: zh-CN):\n\"\"\"\nUser: 你好\nAssistant: 你好！请问有什么我能帮到你的吗？\n\"\"\""
        }
    ],
    "model":"openai:gpt-3.5-turbo",
    "max_tokens":null,
    "temperature":1,
    "presence_penalty":0,
    "top_p":1,
    "frequency_penalty":0
}
```
如需以非Stream(SSE)方式调用，仅支持调用`gpt-3.5-turbo`模型，其余模型均以Stream(SSE)方式提供。

如果您具有相应权限，也可以调用`anthropic:claude-1`等由Anthropic提供的模型，在该API端点下这些模型的响应都被格式化为OpenAI兼容(SSE)。

## 经简化的Chat API
Kamiya同时提供经过简化的Chat API，可按实际需求调用。该API由服务器维护上下文(Context)，Redis缓存时间为48H。
```http request
POST https://p0.kamiya.dev/api/openai/chatgpt/conversation
```
请求示例
```json
{
    "content":"你好",
    "conversationId": "e282d7a0-b183-4c7c-ba9c-3559e2af751f" //首次请求（第一条消息）无需携带，后续消息需携带
}
```
Stream(SSE)响应

响应格式与OpenAI Chat Completion一致，在`[DONE]`发送后还会追加一段由Kamiya提供的`kamiyaInfo`，其中包含
```json
{
    "id": "kamiyaInfo",
    "conversationId": "e282d7a0-b183-4c7c-ba9c-3559e2af751f", // 后续消息中需包含的conversationId
    "message": "[DONE]",
    "fullContent": "请问你想要我帮助你什么方面的问题呢？" // 产生计费的完整回复
}
```
使用该API时无需维护上下文，上下文裁剪与窗口控制均有Kamiya完成。
