# 快速开始

## 基本准备

您应拥有一个 Kamiya ID，在 [Kamiya.Dev](https://www.kamiya.dev) 注册。

## API Key

在注册 Kamiya ID 后，您需要在 [Kamiya ID](https://www.kamiya.dev/account.html) 页面创建与账号关联的API Key来作为访问Kamiya API的凭据。

API Key类似于 `sk-cAPqidPQEZlHQ2T09iKYwOeWz9ZF5VUbbCgSPxNtivnpCV67`。

!> API Key具有关联账户的全部访问权限，请妥善保管。

## 计费

使用Kamiya的服务时会扣除相应的代币，在 [Kamiya.Dev](https://www.kamiya.dev) 中被称为 **魔晶** 。

可以通过 [Kamiya.Dev](https://www.kamiya.dev) 中的 **签到** 或前往 [魔法商店](https://shop.kamiya.dev) 补充账户内的魔晶。
